{
    "name" : "JNet timesheet",
    "version" : "0.1",
    "author" : "Dzmitry Misiuk",
    "category" : "Generic Modules/Others",
    "website" : "http://www.jnet.nl",
    "description": "To create timesheet report by employee and analytic account",
    "depends": ['account', 'hr', 'base', 'hr_attendance', 'hr_timesheet', 'process'],
    "init_xml" : [],
    "data": [
        "employee_analytic_account_timesheet_report.xml",
        "hr_timesheet_print_employee_analytic_account_view.xml"
    ],
    "update_xml" : [],
    "active": True,
    "installable": True
}
