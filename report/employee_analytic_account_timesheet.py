# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import datetime
import time

from openerp.report.interface import report_rml
from openerp import pooler
import openerp.tools as tools

def to_xml(value):
    unicode_value = tools.ustr(value)
    return unicode_value.replace('&', '&amp;').replace('<','&lt;').replace('>','&gt;') \
        .replace('"', '&quot;').replace("'", "&apos;")

def revert_date(value):
    original_date = value.split('-')
    original_date.reverse()
    reverted_date = '-'.join(original_date)
    return reverted_date

class report_custom(report_rml):
    def create_xml(self, cr, uid, ids, data, context):

        analytic_account_xml = "All"
        employee_name_xml = "All"
        customer_name_xml = "All"
        data['name'] = 'my_report_name'

        emp_id = data['form']['employee_id']
        account_id = data['form']['analytic_account_id']
        partner_id = data['form']['partner_id']

        from_date = data['form']['from_date']
        to_date = data['form']['to_date']
        title_xml = "Timesheet by employee, analytic account and customer from %s to %s" % (revert_date(from_date), revert_date(to_date))
        som = datetime.datetime.strptime(from_date, "%Y-%m-%d")
        eom = datetime.datetime.strptime(to_date, "%Y-%m-%d")
        #if som > eom:
        #    raise osv.except_osv('Incorrect Data',"From date should be earlier or same as to date value")

        # for get company name used invoker uid
        company_name = to_xml(pooler.get_pool(cr.dbname).get('res.users').browse(cr,uid,uid).company_id.name)

        header_xml = '''
            <header>
            <date>%s</date>
            <company>%s</company>
            </header>
            ''' % (time.strftime('%d-%m-%Y'), company_name)

        query_recursive_part = '''
            with recursive temp1 ( id,parent_id) AS (
                select t1.id, t1.parent_id from account_analytic_account T1 where T1.id=%s
                union
                select t2.id, t2.parent_id from account_analytic_account T2 inner join temp1 on( temp1.id= t2.parent_id)
            )\n
            '''
        query_select_part = '''
            select line.date, users.id as user_id,
            resource.name as employee_full_name,
            line.name as description,
            account.id as aid,
            account.name as aname,
            line.unit_amount as duration
            from account_analytic_line as line,
            hr_analytic_timesheet as hr,
            resource_resource as resource,
            account_analytic_account as account,
            res_users as users\n
        '''
        query_where_part = '''
            where  hr.line_id = line.id
            and line.account_id=account.id
            and users.id = line.user_id
            and users.id = resource.user_id
            and line.date >= %s and line.date <= %s
        '''

        query_params = (som.strftime('%Y-%m-%d'), eom.strftime('%Y-%m-%d'))
        if partner_id:
            query_params = query_params + (partner_id,)
            query_where_part += '\n and account.partner_id = %s'
            temp_obj = pooler.get_pool(cr.dbname).get('res.partner')
            customer_name_xml = temp_obj.name_get(cr, uid, [partner_id], context)
            customer_name_xml = customer_name_xml[0][1]

        query_str = query_select_part + query_where_part

        if account_id:
            query_params = (account_id,) + query_params
            query_str = query_recursive_part + query_select_part + ", temp1 \n" + query_where_part + "\n and account.id = temp1.id"
            temp_obj = pooler.get_pool(cr.dbname).get('account.analytic.account')
            analytic_account_xml = temp_obj.name_get(cr, uid, [account_id], context)
            analytic_account_xml = analytic_account_xml[0][1]

        if emp_id:
            query_str += '\nand line.user_id=%s'
            emp_obj = pooler.get_pool(cr.dbname).get('hr.employee')
            user_id = emp_obj.browse(cr, uid, emp_id).user_id.id
            query_params = query_params + (user_id,)
            emp_obj = pooler.get_pool(cr.dbname).get('hr.employee')
            employee_name_xml = emp_obj.browse(cr, uid, emp_id).name


        query_str += '\n order by account.id, employee_full_name, date'

        cr.execute(query_str, query_params)
        lines = cr.dictfetchall()

        data_xml = []
        data_xml.append('<activities>')
        activity_str = '''<activity date='%s' user='%s' description='%s' account='%s' duration='%s' />''';
        total_duration = 0
        for line in lines:
            aid = line['aid']
            user_name = line['employee_full_name']
            duration = line['duration']
            temp_obj = pooler.get_pool(cr.dbname).get('account.analytic.account')
            account_full_name = temp_obj.name_get(cr, uid, [aid], context)
            account_full_name = account_full_name[0][1]
            total_duration += duration
            duration = round(duration, 2)
            reverted_date = revert_date(line['date'])
            activity_xml = activity_str % (
                    reverted_date, to_xml(user_name), to_xml(line['description']), to_xml(account_full_name), duration)
            data_xml.append(activity_xml)

        total_duration = round(total_duration, 2)
        data_xml.append('</activities>')
        data_xml.append('''<total>%s</total>''' % total_duration)


        xml = '''<?xml version="1.0" encoding="UTF-8" ?>
        <report>
        %s
        <employee>%s</employee>
        <analytic_account>%s</analytic_account>
        <customer>%s</customer>
        <title>%s</title>
        %s
        </report>
        ''' % (header_xml, employee_name_xml, analytic_account_xml, customer_name_xml, title_xml, '\n'.join(data_xml))
        return xml


report_custom('report.hr.analytical.timesheet_employee_analytic_account', 'employee_analytic_account', '',
              'addons/jnet-ts/report/employee_analytic_account_timesheet.xsl')


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: