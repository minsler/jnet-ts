<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="custom_default.xsl"/>
    <xsl:import href="custom_rml.xsl"/>

    <xsl:template match="/">
        <xsl:call-template name="rml"/>
    </xsl:template>

    <xsl:template name="stylesheet">
        <paraStyle name="normal" fontName="Helvetica" fontSize="6" alignment="left"/>
        <paraStyle name="normal-title" fontName="Helvetica" fontSize="6"/>
        <paraStyle name="title-name" fontName="Helvetica" fontSize="12" alignment="left"/>
        <paraStyle name="employee" fontName="Helvetica-Oblique" fontSize="10" textColor="blue"/>
        <paraStyle name="analyticAccout" fontName="Helvetica-Oblique" fontSize="10" textColor="blue"/>
        <paraStyle name="customer" fontName="Helvetica-Oblique" fontSize="10" textColor="blue"/>
        <paraStyle name="glande" textColor="red" fontSize="7" fontName="Helvetica"/>
        <paraStyle name="normal_people" textColor="green" fontSize="7" fontName="Helvetica"/>
        <paraStyle name="esclave" textColor="purple" fontSize="7" fontName="Helvetica"/>
        <paraStyle name="result" fontName="Helvetica" fontSize="10" alignment="right"/>
        <paraStyle name="table-header" fontName="Helvetica" fontSize="10" alignment="left"/>
        <blockTableStyle id="data-table">
            <blockAlignment value="CENTER" start="0,0" stop="-1,-1"/>
            <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
            <lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,-1"/>
            <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="-1,-1"/>
            <lineStyle kind="LINEAFTER" colorName="black" start="-1,0" stop="-1,-1"/>
            <lineStyle kind="LINEBELOW" colorName="black" start="0,-1" stop="-1,-1"/>
            <blockValign value="TOP"/>
        </blockTableStyle>
    </xsl:template>

    <xsl:template name="story">
        <spacer length="1cm"/>
        <para style="title-name" t="1">
            <xsl:value-of select="/report/title"/>
        </para>
        <spacer length="0.5cm"/>
        <para style="employee">
            Employee: <b><xsl:value-of select="/report/employee"/></b>
        </para>
        <para style="analyticAccout">
            Analytic account: <b><xsl:value-of select="/report/analytic_account"/></b>
        </para>
        <para style="customer">
            Customer: <b><xsl:value-of select="/report/customer"/></b>
        </para>
        <spacer length="1cm"/>
        <blockTable>
            <xsl:attribute name="style">data-table</xsl:attribute>
            <xsl:attribute name="colWidths">2cm,3cm,12cm,5cm,4cm</xsl:attribute>
            <tr>
                <td>
                    <para style="table-header">Date</para>
                </td>
                <td>
                    <para style="table-header">User</para>
                </td>
                <td>
                    <para style="table-header">Description</para>
                </td>
                <td>
                    <para style="table-header">Analytic account</para>
                </td>
                <td>
                    <para style="table-header">Duration, hour(s)</para>
                </td>
            </tr>
            <xsl:apply-templates select="report/activities/activity"/>
        </blockTable>
        <spacer length="1cm"/>
        <para style="result">Total time(in hours):<b><xsl:value-of select="/report/total"/></b>
        </para>
    </xsl:template>

    <xsl:template match="activity">
        <tr>
            <td>
                <para style="normal-title">
                    <xsl:value-of select="attribute::date"/>
                </para>
            </td>
            <td>
                <para style="normal-title">
                    <xsl:value-of select="attribute::user"/>
                </para>
            </td>
            <td>
                <para style="normal-title">
                    <xsl:value-of select="attribute::description"/>
                </para>
            </td>
            <td>
                <para style="normal-title">
                    <xsl:value-of select="attribute::account"/>
                </para>
            </td>
            <td>
                <para style="normal-title">
                    <xsl:value-of select="attribute::duration"/>
                </para>
            </td>
        </tr>
    </xsl:template>

</xsl:stylesheet>