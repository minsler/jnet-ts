import datetime

from osv import osv, fields
from tools.translate import _


def first_day_of_this_month():
    today = datetime.date.today()
    return datetime.date(today.year, today.month, 1)

class employee_analytic_account(osv.osv_memory):
    _name = 'employee_analytic_account'
    _table = 'employee_analytic_account'

    _columns = {
        'from_date': fields.date('From Date', help='From date', required=True),
        'to_date': fields.date('To Date', help='To date', required=True),
        'employee_id': fields.many2one('hr.employee', 'Employee', required=False),
        'analytic_account_id': fields.many2one('account.analytic.account', 'Analytic account', required=False),
        'partner_id': fields.many2one('res.partner', 'Customer', required=False)
    }


    _defaults = {
        'to_date': lambda *a: datetime.date.today().strftime("%Y-%m-%d"), # today
        'from_date': lambda *a: first_day_of_this_month().strftime("%Y-%m-%d") # start of month
    }

    def print_report(self, cr, uid, ids, context=None):
        data = self.read(cr, uid, ids, context=context)[0]
        analytic_account_name = "All"
        employee_name = "All"
        customer_name = "All"

        if data['analytic_account_id']:
            data['analytic_account_id'] = data['analytic_account_id'][0]
            analytic_account_name = data['analytic_account_id']
        if data['employee_id']:
            data['employee_id'] = data['employee_id'][0]
            employee_name = data['employee_id']
        if data['partner_id']:
            data['partner_id'] = data['partner_id'][0]
            customer_name = data['partner_id']
        datas = {
            'ids': [],
            'model': 'employee_analytic_account',
            'form': data
        }

        from_date = data['from_date']
        to_date = data['to_date']
        template_file_name = "Timesheet_Report_Account[%s]_Customer[%s]_Employee[%s]_%s_%s"
        custom_file_name = template_file_name %(analytic_account_name, customer_name, employee_name, from_date, to_date)

        # to set report name use field name
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr.analytical.timesheet_employee_analytic_account',
            'name': custom_file_name,
            'datas': datas,
        }


employee_analytic_account()
